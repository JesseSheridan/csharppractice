﻿using NUnit.Framework;

namespace FizzBuzz {
    [TestFixture]
    public class FizzBuzzTest {

        [Test]
        public void evaluate1ShouldReturn1 () {
            FizzBuzz fizzbuzz = new FizzBuzz ();
            Assert.AreEqual ("1", fizzbuzz.evaluate (1));
        }
    }

    public class FizzBuzz {
        public string evaluate (int number) {
            return "2";
        }
    }
}
