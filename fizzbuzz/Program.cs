﻿using NUnit.Framework;

namespace FizzBuzz {
    [TestFixture]
    public class FizzBuzzTest {

        [Test]
        public void evaluate1ShouldReturn1() {
            FizzBuzz fizzbuzz = new FizzBuzz();
            Assert.AreEqual("1", fizzbuzz.evaluate(1));
        }

        [Test]
        public void evaluate2ShouldReturn2() {
            FizzBuzz fizzbuzz = new FizzBuzz();
            Assert.AreEqual("2", fizzbuzz.evaluate(2));
        }

        [Test]
        public void evaluate3ShouldReturnFizz() {
            FizzBuzz fizzbuzz = new FizzBuzz();
            Assert.AreEqual("fizz", fizzbuzz.evaluate(3));
        }

        [Test]
        public void evaluate6ShouldReturnFizz() {
            FizzBuzz fizzbuzz = new FizzBuzz();
            Assert.AreEqual("fizz", fizzbuzz.evaluate(6));
        }

        [Test]
        public void evaluate5ShouldReturnBuzz() {
            FizzBuzz fizzbuzz = new FizzBuzz();
            Assert.AreEqual("buzz", fizzbuzz.evaluate(5));
        }

        [Test]
        public void evaluate10ShouldReturnBuzz() {
            FizzBuzz fizzbuzz = new FizzBuzz();
            Assert.AreEqual("buzz", fizzbuzz.evaluate(10));
        }

        [Test]
        public void evaluate15ShouldReturnFizzBuzz() {
            FizzBuzz fizzbuzz = new FizzBuzz();
            Assert.AreEqual("fizzbuzz", fizzbuzz.evaluate(15));
        }

        [Test]
        public void evaluate30ShouldReturnFizzBuzz() {
            FizzBuzz fizzbuzz = new FizzBuzz();
            Assert.AreEqual("fizzbuzz", fizzbuzz.evaluate(30));
        }
    }

    public class FizzBuzz {
        public string evaluate(int number) {
            if (number % 15 == 0) {
                return "fizzbuzz";
            }
            if (number % 3 == 0) {
                return "fizz";
            }
            if (number % 5 == 0) {
                return "buzz";
            }
            return number + "";
        }
    }
}
