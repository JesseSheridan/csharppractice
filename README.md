# cSharpPractice

A place for me to practice C#

Install all recommended C# extensions

wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb

sudo dpkg -i packages-microsoft-prod.deb

dotnet new console -o myApp

sudo add-apt-repository universe

sudo apt-get install apt-transport-https

sudo apt-get update

sudo apt-get install dotnet-sdk-2.1

dotnet run

dotnet add package Microsoft.NET.Test.Sdk

dotnet add package Nunit3TestAdapter

dotnet add package NUnit

dotnet test
